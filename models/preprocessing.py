import pandas as pd
import numpy as np
from dateutil.parser import parse


class DataAggregation:

    def __init__(self, data=None, data_type=None):
        self.data = data
        self.data_type = data_type
    
    @staticmethod
    def _number_of_digits(s):

        c = 0
        for elem in s:
            if elem.isdigit():
                c += 1
        return c
    
    @staticmethod
    def _number_of_unique_digits(s):

        unique = set()
        c = 0

        for elem in s:
            if elem.isdigit():
                if elem not in unique:
                    c += 1
                    unique.add(elem)
        return c
    
    @staticmethod
    def _number_of_uppers(s):
    
        c = 0
        for elem in s:
            if elem.isalpha():
                if elem.isupper():
                    c += 1
        return c
    
    @staticmethod
    def _number_of_unique_uppers(s):

        unique = set()
        c = 0

        for elem in s:
            if elem.isalpha():
                if elem.isupper():
                    if elem not in unique:
                        c += 1
                        unique.add(elem)
        return c
    
    @staticmethod
    def _number_of_unique_lowers(s):
        
        c = 0
        unique = set()

        for elem in s:
            if elem.isalpha():
                if elem.islower():
                    if elem not in unique:
                        c += 1
                        unique.add(elem)
        return c
    
    @staticmethod
    def _number_of_symbols(s):
        
        c = 0
        for elem in s:
            if not elem.isalpha() and not elem.isdigit():
                c += 1
        return c
    
    @staticmethod
    def _number_of_unique_symbols(s):
        
        c = 0
        unique = set()

        for elem in s:
            if not elem.isalpha() and not elem.isdigit():
                if elem not in unique:
                    c += 1
                    unique.add(elem)
        return c
    
    @staticmethod
    def _check_order(s):

        s = s.lower()
        row1 = "qwertyuiop"
        row1_r = row1[::-1] # this is an extended splice which steps all characters by a position of -1 which reverses the string
        row2 = "asdfghjkl"
        row2_r = row2[::-1]
        row3 = "zxcvbnm"
        row3_r = row3[::-1]
        #row4="1234567890"
        #row4_r = row4[::-1]
        rows = [row1, row1_r, row2, row2_r, row3, row3_r]#,row4,row4_r] # this allows the code to check for reverse sequences 
        points = 0

        for index in range(0, len(s)-2):
            sequence = s[index:index+3]
            for i in range(0, len(rows)-1):
                if sequence in rows[i]:
                    # if you were to add 'print(sequence)' here it would show all the sequences that have consecutive characters
                    points = 1
        return points
    
    @staticmethod
    def _check_date_in_password(s):
        
        cleaned_s = ''.join([i for i in s if i.isdigit()])

        try: 
            parse(cleaned_s, fuzzy=False)
            return len(cleaned_s)
        
        except OverflowError:
            return 0

        except ValueError:
            return 0
    
    @classmethod
    def make(self, data, data_type):

        if data_type == 'TrainDataFrame' or data_type == 'TestDataFrame':

            data['Password'] = data['Password'].apply(lambda x: str(x))
            data['Length'] = data['Password'].apply(lambda x: len(x))
            data['Digits'] = data['Password'].apply(lambda x: self._number_of_digits(x))
            data['Uppers'] = data['Password'].apply(lambda x: self._number_of_uppers(x))
            data['Symbols'] = data['Password'].apply(lambda x: self._number_of_symbols(x))
            data['Unique_digits'] = data['Password'].apply(lambda x: self._number_of_unique_digits(x))
            data['Unique_uppers'] = data['Password'].apply(lambda x: self._number_of_unique_uppers(x))
            data['Unique_lowers'] = data['Password'].apply(lambda x: self._number_of_unique_lowers(x))
            data['Unique_symbols'] = data['Password'].apply(lambda x: self._number_of_unique_symbols(x))
            data['check_order'] = data['Password'].apply(lambda x: self._check_order(x))
            #data['check_date'] = data['Password'].apply(lambda x: self._check_date_in_password(x))
            
            if data_type == 'TrainDataFrame':
                y = np.log1p(data['Times'])
                X = data.drop(['Times', 'Password'], axis=1)
                return X, y
            else:
                X = data.drop(['Password'], axis=1)
                return X

        elif data_type == 'InputPassword':

            features = [
                len(data),
                self._number_of_digits(data),
                self._number_of_uppers(data),
                self._number_of_symbols(data),
                self._number_of_unique_digits(data),
                self._number_of_unique_uppers(data),
                self._number_of_unique_lowers(data),
                self._number_of_unique_symbols(data),
                self._check_order(data),
                #self._check_date_in_password(data)
            ]

            return pd.DataFrame([features], columns=list(range(9)))
