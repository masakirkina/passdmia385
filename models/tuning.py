import os
import joblib
import pandas as pd
import numpy as np
#mport xgboost as xgb
import lightgbm as lgb
#import catboost as cb
from hyperopt import fmin, tpe, STATUS_OK, STATUS_FAIL, Trials, hp
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error


class HyperOptTuning:

    def __init__(self, X, y):
        lgb_reg_params = {
        'learning_rate' : hp.loguniform('learning_rate', np.log(0.005), np.log(0.2)),
        #'learning_rate':    hp.choice('learning_rate',    np.arange(0.05, 0.31, 0.05)),
        'max_depth':        hp.choice('max_depth', np.arange(2, 20, 1, dtype=int)),
        #'max_depth':       -1,
        'min_child_weight': hp.choice('min_child_weight', np.arange(1, 8, 1, dtype=int)),
        'colsample_bytree': hp.choice('colsample_bytree', np.arange(0.3, 0.8, 0.1)),
        'subsample':        hp.uniform('subsample', 0.8, 1),
        'num_leaves' : hp.choice('num_leaves', np.arange(2, 10, 1, dtype=int)), 
        'max_bin': hp.choice('max_bin', np.arange(32, 255, 1,dtype=int)),
        'min_data_in_leaf': hp.choice('min_data_in_leaf', np.arange(1, 256, 1,dtype=int)),
        'min_data_in_bin': hp.choice('min_data_in_bin', np.arange(1, 256, 1,dtype=int)),
        'min_gain_to_split' : hp.choice('min_gain_to_split', np.arange(0.1, 5, 0.01,dtype=int)),
        'feature_fraction': (0.5,0.8),
        'n_estimators':     200,
        }
        lgb_fit_params = {
        'eval_metric': 'rmse',
        'early_stopping_rounds': 10,
        'verbose': False
        }
        self.lgb_para = {
            'reg_params': lgb_reg_params,
            'fit_params': lgb_fit_params,
            'loss_func': lambda y, pred: np.sqrt(mean_squared_error(y, pred)),
        }
        self.X, self.y = X, y
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(X, y, train_size=0.8, random_state=0)


    def _process(self, fn_name, space, trials, algo, max_evals):
        fn = getattr(self, fn_name)
        try:
            result = fmin(fn=fn, space=space, algo=algo, max_evals=max_evals, trials=trials)
        except Exception as e:
            return {'status': STATUS_FAIL, 'exception': str(e)}
        return result, trials
        

    def lgb_reg(self, para):
        reg = lgb.LGBMRegressor(**para['reg_params'])
        return self.train_reg(reg, para)
    

    def train_reg(self, reg, para):
        reg.fit(self.X_train, self.y_train,
                eval_set=[(self.X_train, self.y_train), (self.X_test, self.y_test)],
                **para['fit_params'])
        pred = reg.predict(self.X_test)
        loss = para['loss_func'](self.y_test, pred)
        return {'loss': loss, 'status': STATUS_OK}
    
    
    def build_model(self):
        lgb_opt = self._process(fn_name='lgb_reg', space=self.lgb_para, trials=Trials(), algo=tpe.suggest, max_evals=2)
        print(lgb_opt)
        return lgb.LGBMRegressor(**lgb_opt)

