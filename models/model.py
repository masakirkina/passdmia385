import os
import joblib
import pandas as pd
import numpy as np
from datetime import datetime
from models.preprocessing import DataAggregation
from models.tuning import HyperOptTuning


class PasswordModel:

    def __init__(self):
        self.data_path = 'data/train.csv/train.csv'
        self.test_path = 'data/Xtest.csv/Xtest.csv'
        self.sample_sub_path = 'data/sample_submission.csv/sample_submission.csv'
        self.model_path = 'trained_models/pwd.model'
        self.data = pd.read_csv(self.data_path)
        self.test = pd.read_csv(self.test_path, index_col='Id')
        self.sample_sub = pd.read_csv(self.sample_sub_path, index_col='Id')
        try:
            self.model = joblib.load(self.model_path)
        except:
            self.model = None


    def _fit(self):
        X, y = DataAggregation.make(data=self.data, data_type='TrainDataFrame')
        tune = HyperOptTuning(X, y)
        self.model = tune.build_model().fit(X, y)
        joblib.dump(self.model, self.model_path)
    

    def predict(self, input_password):
        if not self.model:
            self._fit()
        password_features = DataAggregation.make(data=input_password, data_type='InputPassword')
        prediction_freq_log = self.model.predict(password_features)
        prediction_freq = np.expm1(prediction_freq_log)
        if prediction_freq < 1:
            return 1
        else:
            return prediction_freq[0]


    def kaggle_predict(self):

        now = datetime.now()
        current_datetime = now.strftime('%Y-%m-%d-%H-%M-%S')
        output_path = '../data/submissions/submission_{}.csv'.format(current_datetime)
        X = DataAggregation.make(data=self.test, data_type='TestDataFrame')
        y_pred = np.expm1(self.model.predict(X))
        res = pd.Series(y_pred, name='Times')
        res = res.apply(lambda x: x if x < 1 else 1)
        res.to_csv(output_path, index_label='id')
