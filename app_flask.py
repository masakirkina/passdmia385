import os
import joblib
from flask import Flask, request, render_template
from models.model import PasswordModel

model = PasswordModel()

app = Flask(__name__, template_folder='html_t')

@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        pw = request.form['password_input']
        if not pw: pw = 'qwerty'
        pass_freq = model.predict(pw)
        return render_template('index.html', password=pw, prediction=pass_freq)
    else:
        return render_template('index.html')

if __name__ == '__main__':
    app.run(host='0.0.0.0', threaded=False, debug=False)
